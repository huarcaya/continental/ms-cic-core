import { Injectable } from '@nestjs/common';
import { GradesService } from 'src/grades/services/grades/grades.service';

@Injectable()
export class LanguagesService {
  constructor(private gradesService: GradesService) {}

  async findLanguagesByStudent(pidm: number) {
    return this.gradesService.findLanguagesByStudent(pidm);
  }
}
