import { Column, PrimaryColumn } from 'typeorm';

export class LanguageEntity {
  @PrimaryColumn({
    name: 'IDDependencia',
    type: 'char',
    length: 4,
    nullable: false,
  })
  div: string;

  @PrimaryColumn({
    name: 'IDEscuela',
    type: 'char',
    length: 3,
    nullable: false,
  })
  languageId: string;

  @Column({
    name: 'Nombre',
    type: 'varchar',
    length: 250,
  })
  languageName: string;
}
