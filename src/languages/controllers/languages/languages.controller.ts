import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { CIC_LANGUAGES } from 'src/commons/constants/cic.constant';
import { LanguagesService } from 'src/languages/services/languages/languages.service';
import { StudentAndLanguagePayloadDto } from 'src/commons/dtos/payload.dto';

@Controller('languages')
export class LanguagesController {
  constructor(private languagesService: LanguagesService) {}

  @MessagePattern({ cmd: 'get-languages' })
  async findLanguages() {
    return CIC_LANGUAGES;
  }

  @MessagePattern({ cmd: 'get-student-languages' })
  async findLanguagesByStudent(
    @Payload() payload: StudentAndLanguagePayloadDto,
  ) {
    const { pidm } = payload;

    const languageIdList = await this.languagesService.findLanguagesByStudent(
      pidm,
    );

    const languages = CIC_LANGUAGES.filter((item) =>
      languageIdList.includes(item.id),
    );

    return languages;
  }
}
