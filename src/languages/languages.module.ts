import { Module } from '@nestjs/common';
import { GradesModule } from 'src/grades/grades.module';
import { LanguagesController } from './controllers/languages/languages.controller';
import { LanguagesService } from './services/languages/languages.service';

@Module({
  controllers: [LanguagesController],
  imports: [GradesModule],
  providers: [LanguagesService],
})
export class LanguagesModule {}
