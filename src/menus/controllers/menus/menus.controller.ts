import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';
import { MenusService } from 'src/menus/services/menus/menus.service';

@Controller('menus')
export class MenusController {
  constructor(private menusService: MenusService) {}

  @MessagePattern({ cmd: 'get-menus' })
  async find() {
    return this.menusService.find();
  }
}
