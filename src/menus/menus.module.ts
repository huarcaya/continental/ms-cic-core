import { Module } from '@nestjs/common';
import { MenusService } from './services/menus/menus.service';
import { MenusController } from './controllers/menus/menus.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MenuEntity } from './entities/menu.entity';

@Module({
  imports: [TypeOrmModule.forFeature([MenuEntity], 'pg')],
  providers: [MenusService],
  controllers: [MenusController],
})
export class MenusModule {}
