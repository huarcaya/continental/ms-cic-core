import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MenuEntity } from 'src/menus/entities/menu.entity';
import { Repository } from 'typeorm';

@Injectable()
export class MenusService {
  constructor(
    @InjectRepository(MenuEntity, 'pg')
    private menuRepository: Repository<MenuEntity>,
  ) {}

  async find(): Promise<MenuEntity[]> {
    return await this.menuRepository.find();
  }
}
