import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'menu', database: 'pg' })
export class MenuEntity {
  @PrimaryColumn()
  id: number;

  @Column({
    type: 'varchar',
    length: 250,
    nullable: false,
  })
  name: string;

  @Column({
    type: 'varchar',
    length: 250,
  })
  slug: string;

  @Column({
    type: 'varchar',
    length: 250,
  })
  icon: string;

  @Column({
    default: false,
  })
  enabled: boolean;
}
