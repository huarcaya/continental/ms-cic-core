import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { CIC_LANGUAGE_ID_LIST } from 'src/commons/constants/cic.constant';
import { ClassroomEntity } from 'src/classrooms/entities/classroom.entity';
import { FilterDto } from 'src/commons/dtos/filter.dto';
import {
  FindOptionsWhere,
  In,
  LessThanOrEqual,
  MoreThanOrEqual,
  Repository,
} from 'typeorm';

@Injectable()
export class ClassroomsService {
  constructor(
    @InjectRepository(ClassroomEntity, 'apec')
    private classroomRepository: Repository<ClassroomEntity>,
  ) {}

  async findAll(filterQuery?: FilterDto): Promise<ClassroomEntity[]> {
    const where: FindOptionsWhere<ClassroomEntity> = {
      schoolId: In(CIC_LANGUAGE_ID_LIST),
    };

    if (!filterQuery) {
      filterQuery = {
        limit: 10,
        offset: 0,
        startDate: undefined,
        endDate: undefined,
      };
    }

    const { limit = 10, offset = 0 } = filterQuery;
    const { startDate, endDate } = filterQuery;

    if (startDate && endDate) {
      where.realStartDate = MoreThanOrEqual(startDate);
      where.realEndDate = LessThanOrEqual(endDate);
    }

    return this.classroomRepository.find({
      where,
      take: limit,
      skip: offset,
    });
  }

  async findOne(classroomId: string, campus: string): Promise<ClassroomEntity> {
    return await this.classroomRepository.findOne({
      where: { sectionId: classroomId, campus },
    });
  }
}
