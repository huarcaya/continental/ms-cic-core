import { Controller } from '@nestjs/common';
import { MessagePattern } from '@nestjs/microservices';

import { ClassroomsService } from 'src/classrooms/services/classrooms/classrooms.service';

@Controller('classrooms')
export class ClassroomsController {
  constructor(private classroomsService: ClassroomsService) {}

  @MessagePattern({ cmd: 'get-classrooms' })
  async findAll() {
    return await this.classroomsService.findAll();
  }
}
