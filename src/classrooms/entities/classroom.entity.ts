import { Column, Entity, PrimaryColumn } from 'typeorm';

@Entity({ name: 'tblSeccionC', schema: 'dbo' })
export class ClassroomEntity {
  @PrimaryColumn({
    name: 'IDsede',
    type: 'varchar',
    length: 6,
    nullable: false,
  })
  campus: string;

  @PrimaryColumn({
    name: 'IDPerAcad',
    type: 'char',
    length: 6,
    nullable: false,
  })
  term: string;

  @PrimaryColumn({
    name: 'IDDependencia',
    type: 'char',
    length: 4,
    nullable: false,
  })
  div: string;

  @PrimaryColumn({
    name: 'IDSeccionC',
    type: 'varchar',
    length: 18,
    nullable: false,
  })
  sectionId: string;

  @PrimaryColumn({
    name: 'FecInic',
    type: 'datetime',
    nullable: false,
  })
  startDate: Date;

  @Column({ name: 'FecFin', type: 'datetime' })
  endDate: Date;

  @Column({
    name: 'IDEscuela',
    type: 'varchar',
    length: 3,
    nullable: false,
  })
  schoolId: string;

  @Column({ name: 'FecInicReal', type: 'datetime' })
  realStartDate: Date;

  @Column({ name: 'FecFincReal', type: 'datetime' })
  realEndDate: Date;
}
