import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { ClassroomEntity } from './entities/classroom.entity';
import { ClassroomsController } from './controllers/classrooms/classrooms.controller';
import { ClassroomsService } from './services/classrooms/classrooms.service';

@Module({
  imports: [TypeOrmModule.forFeature([ClassroomEntity], 'apec')],
  controllers: [ClassroomsController],
  providers: [ClassroomsService],
})
export class ClassroomsModule {}
