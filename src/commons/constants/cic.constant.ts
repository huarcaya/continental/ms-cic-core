export const CIC_LANGUAGE_ID_LIST = ['I01', 'I02', 'I03', 'I04', 'I05', 'I06'];

export const CIC_LANGUAGES_DICT = {
  I01: {
    code: 'en',
    id: 'I01',
    name: 'English',
    nameEs: 'Inglés',
  },
  I02: {
    code: 'pt',
    id: 'I02',
    name: 'Portuguese',
    nameEs: 'Portugués',
  },
  I03: {
    code: 'it',
    id: 'I03',
    name: 'Italian',
    nameEs: 'Italiano',
  },
  I04: {
    code: 'fr',
    id: 'I04',
    name: 'French',
    nameEs: 'Francés',
  },
  I05: {
    code: 'qu',
    id: 'I05',
    name: 'Quechua',
    nameEs: 'Quechua',
  },
  I06: {
    code: 'de',
    id: 'I06',
    name: 'German',
    nameEs: 'Alemán',
  },
};

export const CIC_LANGUAGES: Language[] = [
  {
    code: CIC_LANGUAGES_DICT.I01.code,
    id: CIC_LANGUAGES_DICT.I01.id,
    name: CIC_LANGUAGES_DICT.I01.name,
    nameEs: CIC_LANGUAGES_DICT.I01.nameEs,
  },
  {
    code: CIC_LANGUAGES_DICT.I02.code,
    id: CIC_LANGUAGES_DICT.I02.id,
    name: CIC_LANGUAGES_DICT.I02.name,
    nameEs: CIC_LANGUAGES_DICT.I02.nameEs,
  },
  {
    code: CIC_LANGUAGES_DICT.I03.code,
    id: CIC_LANGUAGES_DICT.I03.id,
    name: CIC_LANGUAGES_DICT.I03.name,
    nameEs: CIC_LANGUAGES_DICT.I03.nameEs,
  },
  {
    code: CIC_LANGUAGES_DICT.I04.code,
    id: CIC_LANGUAGES_DICT.I04.id,
    name: CIC_LANGUAGES_DICT.I04.name,
    nameEs: CIC_LANGUAGES_DICT.I04.nameEs,
  },
  {
    code: CIC_LANGUAGES_DICT.I05.code,
    id: CIC_LANGUAGES_DICT.I05.id,
    name: CIC_LANGUAGES_DICT.I05.name,
    nameEs: CIC_LANGUAGES_DICT.I05.nameEs,
  },
  {
    code: CIC_LANGUAGES_DICT.I06.code,
    id: CIC_LANGUAGES_DICT.I06.id,
    name: CIC_LANGUAGES_DICT.I06.name,
    nameEs: CIC_LANGUAGES_DICT.I06.nameEs,
  },
];
