import {
  IsDate,
  IsOptional,
  IsPositive,
  Min,
  ValidateIf,
} from 'class-validator';

export class FilterDto {
  @IsOptional()
  @IsPositive()
  limit: number;

  @IsOptional()
  @Min(0)
  offset: number;

  @IsOptional()
  @IsDate()
  startDate: Date;

  @ValidateIf((item) => item.startDate)
  @IsDate()
  endDate: Date;
}
