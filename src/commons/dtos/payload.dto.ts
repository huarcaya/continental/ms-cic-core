import { IsPositive } from 'class-validator';

export class StudentAndLanguagePayloadDto {
  @IsPositive()
  pidm: number;

  languageId: string;
}
