interface Language {
  code: string;
  id: string;
  name: string;
  nameEs: string;
}
