import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { GradeEntity } from './entities/grade.entity';
import { GradesController } from './controllers/grades/grades.controller';
import { GradesService } from './services/grades/grades.service';

@Module({
  imports: [TypeOrmModule.forFeature([GradeEntity], 'apec')],
  controllers: [GradesController],
  providers: [GradesService],
  exports: [GradesService],
})
export class GradesModule {}
