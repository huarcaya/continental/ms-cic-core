import { Column, Entity, PrimaryColumn } from 'typeorm';
import { Exclude, Expose } from 'class-transformer';

@Entity({ name: 'tblNotasEXT', schema: 'dbo' })
export class GradeEntity {
  @PrimaryColumn({
    name: 'IDsede',
    type: 'varchar',
    length: 6,
    nullable: false,
  })
  campus: string;

  @PrimaryColumn({
    name: 'IDPerAcad',
    type: 'varchar',
    length: 6,
    nullable: false,
  })
  term: string;

  @PrimaryColumn({
    name: 'IDDependencia',
    type: 'char',
    length: 4,
    nullable: false,
  })
  div: string;

  @PrimaryColumn({
    name: 'IDSeccionC',
    type: 'varchar',
    length: 18,
    nullable: false,
  })
  sectionId: string;

  @PrimaryColumn({
    name: 'FecInic',
    type: 'datetime',
    nullable: false,
  })
  startDate: Date;

  @PrimaryColumn({
    name: 'IDEscuela',
    type: 'varchar',
    length: 3,
    nullable: false,
  })
  schoolId: string;

  @PrimaryColumn({
    name: 'IDCursoEXT',
    type: 'char',
    length: 5,
    nullable: false,
  })
  extCourseId: string;

  @PrimaryColumn({
    name: 'IDDocenteExt',
    type: 'varchar',
    length: 5,
    nullable: false,
  })
  extTeacherId: string;

  @PrimaryColumn({
    name: 'IDAlumno',
    type: 'varchar',
    length: 10,
    nullable: false,
  })
  studentId: string;

  @PrimaryColumn({
    name: 'fechaexamen',
    type: 'datetime',
    nullable: false,
  })
  examDate: Date;

  @Column({
    name: 'Pidm',
    type: 'int',
    nullable: false,
  })
  pidm: number;

  @Exclude()
  @Column({
    name: 'n50',
    type: 'int',
  })
  n50: number;

  @Exclude()
  @Column({
    name: 'n51',
    type: 'int',
  })
  n51: number;

  @Expose()
  get avg() {
    return this.n51 ?? this.n50;
  }
}
