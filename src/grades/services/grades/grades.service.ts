import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';

import { FindOptionsWhere, MoreThanOrEqual, Repository } from 'typeorm';

import { FilterDto } from 'src/commons/dtos/filter.dto';
import { GradeEntity } from 'src/grades/entities/grade.entity';

@Injectable()
export class GradesService {
  constructor(
    @InjectRepository(GradeEntity, 'apec')
    private gradeRepository: Repository<GradeEntity>,
  ) {}

  async findByStudentAndLanguage(
    pidm: number,
    languageId: string,
    filterQuery?: FilterDto,
  ): Promise<GradeEntity[]> {
    const where: FindOptionsWhere<GradeEntity> = {
      pidm,
      schoolId: languageId,
    };

    const whereN50: FindOptionsWhere<GradeEntity> = {
      ...where,
      n50: MoreThanOrEqual(0),
    };

    const whereN51: FindOptionsWhere<GradeEntity> = {
      ...where,
      n51: MoreThanOrEqual(0),
    };

    if (!filterQuery) {
      filterQuery = {
        limit: 10,
        offset: 0,
        startDate: undefined,
        endDate: undefined,
      };
    }

    const { limit, offset } = filterQuery;

    return this.gradeRepository.find({
      where: [whereN50, whereN51],
      take: limit,
      skip: offset,
      order: { startDate: 'DESC' },
    });
  }

  async findLanguagesByStudent(pidm: number) {
    const where: FindOptionsWhere<GradeEntity> = {
      pidm,
    };

    const languages = await this.gradeRepository.find({
      where: [
        { ...where, n50: MoreThanOrEqual(0) },
        { ...where, n51: MoreThanOrEqual(0) },
      ],
      select: ['schoolId'],
    });

    const lang: string[] = [];

    languages.forEach((item) => lang.push(item.schoolId));

    return [...new Set(lang)];
  }
}
