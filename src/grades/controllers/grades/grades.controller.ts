import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';

import { GradesService } from 'src/grades/services/grades/grades.service';
import { StudentAndLanguagePayloadDto } from 'src/commons/dtos/payload.dto';

@Controller('grades')
export class GradesController {
  constructor(private gradesService: GradesService) {}

  @MessagePattern({ cmd: 'student-language-grade' })
  async findByStudentAndLanguage(
    @Payload() payload: StudentAndLanguagePayloadDto,
  ) {
    const { pidm, languageId } = payload;

    return await this.gradesService.findByStudentAndLanguage(pidm, languageId);
  }
}
