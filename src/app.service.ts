import { Injectable } from '@nestjs/common';

@Injectable()
export class AppService {
  getHello(): string {
    return 'Hello World!';
  }

  getHumans() {
    return {
      team: [
        {
          name: 'Gustavo Huarcaya',
          role: 'Full-stack',
          email: 'diavolo [at] gahd.net',
          site: 'https://gahd.net',
        },
      ],
      thanks: [
        {
          from: 'Gustavo Huarcaya',
          to: ['Yhuvali', 'Daniel', 'Amy'],
        },
      ],
      site: [
        {
          standards: ['JSON'],
        },
        {
          components: ['NestJS', 'PostgreSQL'],
        },
        {
          software: ['vscode', 'DBeaver', 'insomnia'],
        },
      ],
    };
  }
}
