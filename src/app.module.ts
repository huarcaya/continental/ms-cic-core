import { ConfigModule } from '@nestjs/config';
import { Module } from '@nestjs/common';

import * as Joi from 'joi';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ClassroomsModule } from './classrooms/classrooms.module';
import { CommonsModule } from './commons/commons.module';
import { DatabaseModule } from './database/database.module';
import { environments } from './environments';
import { GradesModule } from './grades/grades.module';
import { LanguagesModule } from './languages/languages.module';
import { MenusModule } from './menus/menus.module';
import config from './config';

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: environments[process.env.NODE_ENV] || '.env',
      load: [config],
      isGlobal: true,
      validationSchema: Joi.object({
        API_KEY: Joi.string().required(),
        PORT: Joi.number().required(),
        JWT_ACCESS_TOKEN_SECRET: Joi.string().required(),
      }),
    }),
    ClassroomsModule,
    CommonsModule,
    DatabaseModule,
    GradesModule,
    LanguagesModule,
    MenusModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
