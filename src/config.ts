import { registerAs } from '@nestjs/config';

export default registerAs('config', () => {
  return {
    apiKey: process.env.API_KEY,
    jwtAccessSecret: process.env.JWT_ACCESS_TOKEN_SECRET,
    jwtAccessExp: process.env.JWT_ACCESS_TOKEN_EXPIRATION_TIME,
    jwtRefreshSecret: process.env.JWT_REFRESH_TOKEN_SECRET,
    jwtRefreshExp: process.env.JWT_REFRESH_TOKEN_EXPIRATION_TIME,
    mssql: {
      dbApec: process.env.MSSQL_DB,
      host: process.env.MSSQL_HOST,
      port: parseInt(process.env.MSSQL_PORT, 10),
      user: process.env.MSSQL_USER,
      password: process.env.MSSQL_PASSWORD,
    },
    postgres: {
      dbName: process.env.POSTGRES_DB,
      port: parseInt(process.env.POSTGRES_PORT, 10),
      password: process.env.POSTGRES_PASSWORD,
      user: process.env.POSTGRES_USER,
      host: process.env.POSTGRES_HOST,
    },
  };
});
